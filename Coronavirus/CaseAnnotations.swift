//
//  CaseAnnotations.swift
//  Coronavirus
//
//  Created by Tims on 14/03/2020.
//  Copyright © 2020 Tims. All rights reserved.
//

import Foundation
import MapKit

class CaseAnnotations: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String?,
         subtitle: String?,
         coordinate: CLLocationCoordinate2D) {
        
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
    }
}
