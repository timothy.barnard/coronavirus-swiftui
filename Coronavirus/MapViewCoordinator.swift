//
//  MapViewCoordinator.swift
//  Coronavirus
//
//  Created by Tims on 14/03/2020.
//  Copyright © 2020 Tims. All rights reserved.
//

import Foundation
import MapKit

class MapViewCoordinator: NSObject, MKMapViewDelegate {
    
    var mapViewController: MapView
    
    init(_ mapView: MapView) {
        self.mapViewController = mapView
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "anno")
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "anno")
            annotationView?.canShowCallout = true
        }
        
        let subTitle: UILabel = .init()
        subTitle.translatesAutoresizingMaskIntoConstraints = false
        subTitle.text = annotation.subtitle ?? "NA"
        subTitle.numberOfLines = 0
        
        annotationView?.detailCalloutAccessoryView = subTitle
        return annotationView
    }
    
}
