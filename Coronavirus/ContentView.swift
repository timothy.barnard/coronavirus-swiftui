//
//  ContentView.swift
//  Coronavirus
//
//  Created by Tims on 14/03/2020.
//  Copyright © 2020 Tims. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var coronaCases: CoronaObservable = .init()
    
    var body: some View {
        VStack{
            Text("COVID-19 Data")
                .font(.headline)
                .padding()
            
            HStack{
                Text("Total Cases\n\(coronaCases.coronaOutbreak.totalCases)")
                .multilineTextAlignment(.center)
                Text("Total Recovered\n\(coronaCases.coronaOutbreak.totalRecovered)")
                .multilineTextAlignment(.center)
                Text("Total Deaths\n\(coronaCases.coronaOutbreak.totalDeaths)")
                .multilineTextAlignment(.center)
            }
            MapView(anotations: self.coronaCases.caseAnnotations, totalCount: Int(coronaCases.coronaOutbreak.totalCases) ?? 0)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
