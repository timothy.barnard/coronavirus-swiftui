//
//  MapView.swift
//  Coronavirus
//
//  Created by Tims on 14/03/2020.
//  Copyright © 2020 Tims. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    
    var anotations: [CaseAnnotations]
    var totalCount: Int = 0
    
    func makeCoordinator() -> MapViewCoordinator {
        return MapViewCoordinator(self)
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView(frame: .zero)
        mapView.showsCompass = true
        mapView.delegate = context.coordinator
        return mapView
    }
    
    func updateUIView(_ mapView: MKMapView, context: Context) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(self.anotations)
        
        if let firstAnnotation = self.anotations.first {
            mapView.selectAnnotation(firstAnnotation, animated: true)
        }
    }
}
