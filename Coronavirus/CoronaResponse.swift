//
//  CoronaResponse.swift
//  Coronavirus
//
//  Created by Tims on 14/03/2020.
//  Copyright © 2020 Tims. All rights reserved.
//

import Foundation


struct CoronaResponse: Decodable {
    public var features: [CoronaCases]
}

struct CoronaCases: Decodable {
    public var attributes: CaseAttributes
}

struct CaseAttributes: Decodable {
    let confirmed : Int?
    let countryRegion : String?
    let deaths : Int?
    let lat : Double?
    let longField : Double?
    let provinceState : String?
    let recovered : Int?

    enum CodingKeys: String, CodingKey {
        case confirmed = "Confirmed"
        case countryRegion = "Country_Region"
        case deaths = "Deaths"
        case lat = "Lat"
        case longField = "Long_"
        case provinceState = "Province_State"
        case recovered = "Recovered"
    }
}
